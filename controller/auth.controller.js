const config = require("../config/auth.config");
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
var moment = require("moment");
exports.signup = (req, res) => {
    // Save User to Database
    let {
      code_staff,
      identy_card,
      firt_name,
      last_name,
      gender,
      address,
      phone_number,
      email,
      parts_code_part,
      password,
      role,
    } = req.body;
    Staffs.create({
      code_staff,
      identy_card,
      firt_name,
      last_name,
      gender,
      address,
      phone_number,
      parts_code_part,
      role,
      email: email,
      password: bcrypt.hashSync(password, 8),
      deleted: 0,
    })
      .then((user) => {
        Staffs.findAll({
          attributes: [
            "code_staff",
            "firt_name",
            "last_name",
            "gender",
            "address",
            "phone_number",
            "email",
            "role",
            "createdAt",
            "updatedAt",
            "deleted",
          ],
          include: [
            {
              model: Part,
              as: "detail_info_part",
            },
          ],
        }).then((result) =>
          res.status(200).send({ total: result.length, data: result })
        );
      })
      .catch((err) => {
        res.status(500).send({ message: err.message });
      });
  };
  exports.login = (req, res) => {
    let { email, password } = req.body;
    Staffs.findOne({
      attributes: [
        "code_staff",
        "email",
        "firt_name",
        "last_name",
        "password",
        "role",
        "createdAt",
        "updatedAt",
        "deleted",
      ],
      where: {
        email: email,
        deleted: 0,
      },
    })
      .then((user) => {
        if (!user) {
          return res.status(404).send({ message: "User Not found." });
        }
  
        var passwordIsValid = bcrypt.compareSync(password, user.password);
  
        if (!passwordIsValid) {
          return res.status(401).send({
            accessToken: null,
            message: "Invalid Password!",
          });
        }
  
        var token = jwt.sign({ id: user.email }, config.secret, {
          expiresIn: "30d", // 24 hours
        });
  
        res.status(200).send({
          code: user.code_staff,
          email: user.email,
          name: user.firt_name + " " + user.last_name,
          roles: user.role,
          accessToken: token,
        });
      })
      .catch((err) => {
        res.status(500).send({ message: err.message });
      });
  };