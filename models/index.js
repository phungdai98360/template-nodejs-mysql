global.Sequelize = require("sequelize");
global.sequelize = new Sequelize("hotel_manager_v1", "root", "mixaolanot123", {
  host: "127.0.0.1",
  //logging: false,
  dialect: "mysql",
  pool: {
    max: 50,
    min: 0,
    idle: 10000,
  },
});

global.Users = require("./users");
global.Roles = require("./roles");
//join table
Roles.hasMany(Users, {
  sourceKey: "id",
  foreignKey: "role_id",
  as: "list_user",
});
Users.belongsTo(Roles, {
  sourceKey: "id",
  foreignKey: "role_id",
  as: "detail_role",
});
